import Vue from 'vue'
import Router from 'vue-router'
import SearchWindow from './components/SearchWindow'
import AuthorList from './components/AuthorList'
import BookList from './components/BookList'

Vue.use(Router)

export default new Router({
    routes: [
        {
            path: '/',
            redirect: '/search'
        },
        {
            path: '/search',
            name: 'SearchWindow',
            component: SearchWindow
        },
        {
            path: '/authors',
            name: 'AuthorList',
            component: AuthorList
        },
        {
            path: '/books',
            name: 'BookList',
            component: BookList
        }
    ]
})

/*
По поводу ограничения:
"К грузовику и прицепу привязано различные датчики, до 3-х (может не быть связи)"

Если необходимо обрабатывать это условие исходя из ограничения в 1 элемент(1 столбец)
нужно договориться об организации списка (например через запятую: 1,2,3)
Написать тригер который поочередно проверит каждый из элементов на соответсвие словарю sensors
и учтет превышение лимита в 3 эелемента:
declare
v_cnt number(1);
begin
if :NEW.sens_sens_id is not null then
    if regexp_count(:NEW.sens_sens_id, ',') + 1 <= 3 then
        for rec in (select regexp_substr(:NEW.sens_sens_id, '[^.*,]+', 1, level) sens_id
                      from dual
                   connect by level <= regexp_count(:NEW.sens_sens_id, ',') + 1) loop
            select count(*)
              into v_cnt
              from sensors s
             where s.sens_id = rec.sens_id;
            if v_cnt = 0 then
                raise_application_error(-20000, 'Датчика не существует');
            end if;
        end loop;
    end if;
end if;
exception
  when others then
    raise_application_error(-20000, 'Что-то пошло не так');
end;
*/

CREATE TABLE TRUCKS (
  TRUCK_ID INT NOT NULL,
  GNUM VARCHAR2(15) UNIQUE NOT NULL,
  DESCRIPT VARCHAR2(500),
  AK_AK_ID INT NOT NULL,
  TRAILER_TRAILER_ID INT,
  GPS_GPS_ID INT,
  TKM_TKM_ID INT NOT NULL,
  SENS_SENS_ID1 INT,
  SENS_SENS_ID2 INT,
  SENS_SENS_ID3 INT,
  DRIVER_DRIVER_ID INT,
  CONSTRAINT TRUCKS_PK PRIMARY KEY (TRUCK_ID));

CREATE SEQUENCE TRUCKS_TRUCK_ID_SEQ;

CREATE TRIGGER BI_TRUCKS_TRUCK_ID
  BEFORE INSERT ON TRUCKS
  FOR EACH ROW
BEGIN
  SELECT TRUCKS_TRUCK_ID_SEQ.NEXTVAL INTO :NEW.TRUCK_ID FROM DUAL;
END;
/

CREATE OR REPLACE TRIGGER BI_TRUCKS_GNUM_CK
  BEFORE INSERT OR UPDATE ON TRUCKS
  FOR EACH ROW
BEGIN
  IF REGEXP_REPLACE(UPPER(:NEW.GNUM), '[АВЕКМНОРСТУХ]{1}\d{3}[АВЕКМНОРСТУХ]{2}\d{2,3}') IS NOT NULL THEN
    RAISE_APPLICATION_ERROR(-20000, :NEW.GNUM || ' - Гос.номер не соответствует маске В111ВВ93 или В111ВВ193');
  END IF;
END;
/

CREATE OR REPLACE TRIGGER BI_TRUCKS_DRIVER_ID
  BEFORE INSERT OR UPDATE ON TRUCKS
  FOR EACH ROW
DECLARE
V_DONE NUMBER(1) := 0;
BEGIN
  IF :NEW.DRIVER_DRIVER_ID IS NOT NULL THEN
    SELECT CASE
             WHEN D.AK_AK_ID = :NEW.AK_AK_ID THEN
               1
             ELSE
               0
           END CASE
      INTO V_DONE
      FROM DRIVERS D
     WHERE D.DRIVER_ID = :NEW.DRIVER_DRIVER_ID;
    IF V_DONE = 0 THEN
      RAISE_APPLICATION_ERROR(-20000, 'ak_id водителя не соответствует ak_id грузовика');
    END IF;
  END IF;
EXCEPTION
  WHEN NO_DATA_FOUND THEN
    RAISE_APPLICATION_ERROR(-20000, 'ak_id не найден');
END;
/

CREATE OR REPLACE TRIGGER BI_TRUCKS_TRAILER_ID
  BEFORE INSERT OR UPDATE ON TRUCKS
  FOR EACH ROW
DECLARE
V_DONE NUMBER(1) := 0;
BEGIN
  SELECT COUNT(*)
    INTO V_DONE
    FROM TRAILERS T, AK
   WHERE T.TRAILER_ID = :NEW.TRAILER_TRAILER_ID
     AND AK.AK_ID = T.AK_AK_ID
     AND AK.ATP_ATP_ID IN (SELECT AK.ATP_ATP_ID
                             FROM AK
                            WHERE AK.AK_ID = :NEW.AK_AK_ID);

  IF V_DONE = 0 THEN
    RAISE_APPLICATION_ERROR(-20000, 'atp_id прицепа не соответствует atp_id грузовика');
  END IF;
EXCEPTION
  WHEN NO_DATA_FOUND THEN
    RAISE_APPLICATION_ERROR(-20000, 'atp_id не найден');
END;
/

CREATE OR REPLACE TRIGGER BI_TRUCKS_LOG
  AFTER UPDATE OR INSERT ON TRUCKS
  FOR EACH ROW
DECLARE
 PRAGMA AUTONOMOUS_TRANSACTION;
 TXT CLOB;
BEGIN
  IF INSERTING THEN
    txt := 'New row:' || 'truck_id:'           || :NEW.truck_id           || ' '
                      || 'gnum:'               || :NEW.gnum               || ' '
                      || 'descript:'           || :NEW.descript           || ' '
                      || 'ak_ak_id:'           || :NEW.ak_ak_id           || ' '
                      || 'trailer_trailer_id:' || :NEW.trailer_trailer_id || ' '
                      || 'gps_gps_id:'         || :NEW.gps_gps_id         || ' '
                      || 'tkm_tkm_id:'         || :NEW.tkm_tkm_id         || ' '
                      || 'sens_sens_id1:'      || :NEW.sens_sens_id1      || ' '
                      || 'sens_sens_id2:'      || :NEW.sens_sens_id2      || ' '
                      || 'sens_sens_id3:'      || :NEW.sens_sens_id3      || ' '
                      || 'driver_driver_id:'   || :NEW.driver_driver_id
                      || chr(10)
                      || chr(13);
  ELSIF UPDATING THEN
    txt := txt || 'Old row: '
               || 'truck_id:'           || :OLD.truck_id           || ' '
               || 'gnum:'               || :OLD.gnum               || ' '
               || 'descript:'           || :OLD.descript           || ' '
               || 'ak_ak_id:'           || :OLD.ak_ak_id           || ' '
               || 'trailer_trailer_id:' || :OLD.trailer_trailer_id || ' '
               || 'gps_gps_id:'         || :OLD.gps_gps_id         || ' '
               || 'tkm_tkm_id:'         || :OLD.tkm_tkm_id         || ' '
               || 'sens_sens_id1:'      || :OLD.sens_sens_id1      || ' '
               || 'sens_sens_id2:'      || :OLD.sens_sens_id2      || ' '
               || 'sens_sens_id3:'      || :OLD.sens_sens_id3      || ' '
               || 'driver_driver_id:'   || :OLD.driver_driver_id;
  END IF;

  INSERT INTO TT_LOGS(LOG_TABLE, LOG_TEXT) VALUES('TRUCKS', TXT);
  COMMIT;
END;
/

CREATE TABLE TRAILERS (
  TRAILER_ID NUMBER(15, 0) NOT NULL,
  GNUM VARCHAR2(15) UNIQUE NOT NULL,
  DESCRIPT VARCHAR2(500),
  AK_AK_ID INT NOT NULL,
  GPS_GPS_ID INT,
  TRM_TRM_ID INT NOT NULL,
  SENS_SENS_ID1 INT,
  SENS_SENS_ID2 INT,
  SENS_SENS_ID3 INT,
  CONSTRAINT TRAILERS_PK PRIMARY KEY (TRAILER_ID));

CREATE SEQUENCE TRAILERS_TRAILER_ID_SEQ;

CREATE TRIGGER BI_TRAILERS_TRAILER_ID
  BEFORE INSERT ON TRAILERS
  FOR EACH ROW
BEGIN
  SELECT TRAILERS_TRAILER_ID_SEQ.NEXTVAL INTO :NEW.TRAILER_ID FROM DUAL;
END;
/

CREATE OR REPLACE TRIGGER BI_TRAILERS_GNUM_CK
  BEFORE INSERT OR UPDATE ON TRAILERS
  FOR EACH ROW
BEGIN
  IF REGEXP_REPLACE(UPPER(:NEW.GNUM), '[АВЕКМНОРСТУХ]{2}\d{6,7}') IS NOT NULL THEN
    RAISE_APPLICATION_ERROR(-20000, :NEW.GNUM || ' - Гос.номер не соответствует маске АА112293 или АА1122193');
  END IF;
END;
/

CREATE OR REPLACE TRIGGER BI_TRAILERS_LOG
  AFTER UPDATE OR INSERT ON TRAILERS
  FOR EACH ROW
DECLARE
  PRAGMA AUTONOMOUS_TRANSACTION;
  TXT CLOB;
BEGIN
  IF INSERTING THEN
    txt := 'New row:' || 'truck_id:'           || :NEW.trailer_id         || ' '
                      || 'gnum:'               || :NEW.gnum               || ' '
                      || 'descript:'           || :NEW.descript           || ' '
                      || 'ak_ak_id:'           || :NEW.ak_ak_id           || ' '
                      || 'gps_gps_id:'         || :NEW.gps_gps_id         || ' '
                      || 'tkm_tkm_id:'         || :NEW.trm_trm_id         || ' '
                      || 'sens_sens_id1:'      || :NEW.sens_sens_id1      || ' '
                      || 'sens_sens_id2:'      || :NEW.sens_sens_id2      || ' '
                      || 'sens_sens_id3:'      || :NEW.sens_sens_id3
                      || chr(10)
                      || chr(13);
  ELSIF UPDATING THEN
    txt := txt || 'Old row: '
               || 'truck_id:'           || :OLD.trailer_id         || ' '
               || 'gnum:'               || :OLD.gnum               || ' '
               || 'descript:'           || :OLD.descript           || ' '
               || 'ak_ak_id:'           || :OLD.ak_ak_id           || ' '
               || 'gps_gps_id:'         || :OLD.gps_gps_id         || ' '
               || 'tkm_tkm_id:'         || :OLD.trm_trm_id         || ' '
               || 'sens_sens_id1:'      || :OLD.sens_sens_id1      || ' '
               || 'sens_sens_id2:'      || :OLD.sens_sens_id2      || ' '
               || 'sens_sens_id3:'      || :OLD.sens_sens_id3;
  END IF;

  INSERT INTO TT_LOGS(LOG_TABLE, LOG_TEXT) VALUES('TRUCKS', TXT);
  COMMIT;
END;
/

CREATE TABLE TRUCK_MODELS (
  TKM_ID INT NOT NULL,
  TRUCK_NAME VARCHAR2(500) NOT NULL,
  CAP NUMBER,
  CARCAP NUMBER,
  P1 VARCHAR2(500),
  P2 VARCHAR2(500),
  CONSTRAINT TRUCK_MODELS_PK PRIMARY KEY (TKM_ID));

CREATE SEQUENCE TRUCK_MODELS_TKM_ID_SEQ;

CREATE TRIGGER BI_TRUCK_MODELS_TKM_ID
  BEFORE INSERT ON TRUCK_MODELS
  FOR EACH ROW
BEGIN
  SELECT TRUCK_MODELS_TKM_ID_SEQ.NEXTVAL INTO :NEW.TKM_ID FROM DUAL;
END;
/

CREATE TABLE TRAILER_MODELS (
  TRM_ID INT NOT NULL,
  TRAILER_NAME VARCHAR2(500) NOT NULL,
  CAP NUMBER,
  CARCAP NUMBER,
  P1 VARCHAR2(500),
  P2 VARCHAR2(500),
  CONSTRAINT TRAILER_MODELS_PK PRIMARY KEY (TRM_ID));

CREATE SEQUENCE TRAILER_MODELS_TRM_ID_SEQ;

CREATE TRIGGER BI_TRAILER_MODELS_TRM_ID
  BEFORE INSERT ON TRAILER_MODELS
  FOR EACH ROW
BEGIN
  SELECT TRAILER_MODELS_TRM_ID_SEQ.NEXTVAL INTO :NEW.TRM_ID FROM DUAL;
END;
/

CREATE TABLE ATP (
  ATP_ID INT NOT NULL,
  ATP_NAME VARCHAR2(500) UNIQUE NOT NULL,
  CONSTRAINT ATP_PK PRIMARY KEY (ATP_ID));

CREATE SEQUENCE ATP_ATP_ID_SEQ;

CREATE TRIGGER BI_ATP_ATP_ID
  BEFORE INSERT ON ATP
  FOR EACH ROW
BEGIN
  SELECT ATP_ATP_ID_SEQ.NEXTVAL INTO :NEW.ATP_ID FROM DUAL;
END;
/

CREATE TABLE AK (
  AK_ID INT NOT NULL,
  AK_NAME VARCHAR2(500) UNIQUE NOT NULL,
  ATP_ATP_ID INT NOT NULL,
  CONSTRAINT AK_PK PRIMARY KEY (AK_ID));

CREATE SEQUENCE AK_AK_ID_SEQ;

CREATE TRIGGER BI_AK_AK_ID
  BEFORE INSERT ON AK
  FOR EACH ROW
BEGIN
  SELECT AK_AK_ID_SEQ.NEXTVAL INTO :NEW.AK_ID FROM DUAL;
END;
/

CREATE TABLE GPS_UNITS (
  GPS_ID INT NOT NULL,
  CALL_SIGN VARCHAR2(500) UNIQUE NOT NULL,
  CONSTRAINT GPS_UNITS_PK PRIMARY KEY (GPS_ID));

CREATE SEQUENCE GPS_UNITS_GPS_ID_SEQ;

CREATE TRIGGER BI_GPS_UNITS_GPS_ID
  BEFORE INSERT ON GPS_UNITS
  FOR EACH ROW
BEGIN
  SELECT GPS_UNITS_GPS_ID_SEQ.NEXTVAL INTO :NEW.GPS_ID FROM DUAL;
END;
/

CREATE TABLE SENSOR (
  SENS_ID INT NOT NULL,
  SENS_NAME VARCHAR2(500) UNIQUE NOT NULL,
  CONSTRAINT SENSOR_PK PRIMARY KEY (SENS_ID));

CREATE SEQUENCE SENSOR_SENS_ID_SEQ;

CREATE TRIGGER BI_SENSOR_SENS_ID
  BEFORE INSERT ON SENSOR
  FOR EACH ROW
BEGIN
  SELECT SENSOR_SENS_ID_SEQ.NEXTVAL INTO :NEW.SENS_ID FROM DUAL;
END;
/

CREATE TABLE DRIVERS (
  DRIVER_ID INT NOT NULL,
  FNAME VARCHAR2(500) NOT NULL,
  INAME VARCHAR2(500) NOT NULL,
  ONAME VARCHAR2(500) NOT NULL,
  ADDRESS VARCHAR2(500) NOT NULL,
  PHONE VARCHAR2(500) NOT NULL,
  AK_AK_ID INT,
  CONSTRAINT DRIVERS_PK PRIMARY KEY (DRIVER_ID));

CREATE SEQUENCE DRIVERS_DRIVER_ID_SEQ;

CREATE TRIGGER BI_DRIVERS_DRIVER_ID
  BEFORE INSERT ON DRIVERS
  FOR EACH ROW
BEGIN
  SELECT DRIVERS_DRIVER_ID_SEQ.NEXTVAL INTO :NEW.DRIVER_ID FROM DUAL;
END;
/

CREATE TABLE TT_LOGS (
  LOG_ID INT NOT NULL,
  LOG_TABLE VARCHAR2(100),
  LOG_TEXT CLOB,
  CONSTRAINT TT_LOGS_PK PRIMARY KEY (LOG_ID));

CREATE SEQUENCE TT_LOGS_LOG_ID_SEQ;

CREATE TRIGGER BI_TT_LOGS_LOG_ID
  BEFORE INSERT ON TT_LOGS
  FOR EACH ROW
BEGIN
  SELECT TT_LOGS_LOG_ID_SEQ.NEXTVAL INTO :NEW.LOG_ID FROM DUAL;
END;
/


ALTER TABLE TRUCKS ADD CONSTRAINT TRUCKS_FK0 FOREIGN KEY (AK_AK_ID) REFERENCES AK(AK_ID);
ALTER TABLE TRUCKS ADD CONSTRAINT TRUCKS_FK1 FOREIGN KEY (TRAILER_TRAILER_ID) REFERENCES TRAILERS(TRAILER_ID);
ALTER TABLE TRUCKS ADD CONSTRAINT TRUCKS_FK2 FOREIGN KEY (GPS_GPS_ID) REFERENCES GPS_UNITS(GPS_ID);
ALTER TABLE TRUCKS ADD CONSTRAINT TRUCKS_FK3 FOREIGN KEY (TKM_TKM_ID) REFERENCES TRUCK_MODELS(TKM_ID);
ALTER TABLE TRUCKS ADD CONSTRAINT TRUCKS_FK4 FOREIGN KEY (SENS_SENS_ID1) REFERENCES SENSOR(SENS_ID);
ALTER TABLE TRUCKS ADD CONSTRAINT TRUCKS_FK5 FOREIGN KEY (SENS_SENS_ID2) REFERENCES SENSOR(SENS_ID);
ALTER TABLE TRUCKS ADD CONSTRAINT TRUCKS_FK6 FOREIGN KEY (SENS_SENS_ID3) REFERENCES SENSOR(SENS_ID);
ALTER TABLE TRUCKS ADD CONSTRAINT TRUCKS_FK7 FOREIGN KEY (DRIVER_DRIVER_ID) REFERENCES DRIVERS(DRIVER_ID);

ALTER TABLE TRAILERS ADD CONSTRAINT TRAILERS_FK0 FOREIGN KEY (AK_AK_ID) REFERENCES AK(AK_ID);
ALTER TABLE TRAILERS ADD CONSTRAINT TRAILERS_FK1 FOREIGN KEY (GPS_GPS_ID) REFERENCES GPS_UNITS(GPS_ID);
ALTER TABLE TRAILERS ADD CONSTRAINT TRAILERS_FK2 FOREIGN KEY (TRM_TRM_ID) REFERENCES TRAILER_MODELS(TRM_ID);
ALTER TABLE TRAILERS ADD CONSTRAINT TRAILERS_FK3 FOREIGN KEY (SENS_SENS_ID1) REFERENCES SENSOR(SENS_ID);
ALTER TABLE TRAILERS ADD CONSTRAINT TRAILERS_FK4 FOREIGN KEY (SENS_SENS_ID2) REFERENCES SENSOR(SENS_ID);
ALTER TABLE TRAILERS ADD CONSTRAINT TRAILERS_FK5 FOREIGN KEY (SENS_SENS_ID3) REFERENCES SENSOR(SENS_ID);

ALTER TABLE AK ADD CONSTRAINT AK_FK0 FOREIGN KEY (ATP_ATP_ID) REFERENCES ATP(ATP_ID);

ALTER TABLE DRIVERS ADD CONSTRAINT DRIVERS_FK0 FOREIGN KEY (AK_AK_ID) REFERENCES AK(AK_ID);

comment on column trucks.gnum is 'Гос.Номер грузовика';
comment on column trucks.descript is 'Описание';

comment on column trailers.gnum is 'Гос.Номер прицепа';
comment on column trailers.descript is 'Описание';

comment on column truck_models.truck_name is 'Наименование';
comment on column truck_models.cap is 'Вместимость';
comment on column truck_models.carcap is 'Грузоподъемность';
comment on column truck_models.p1 is 'Признак 1';
comment on column truck_models.p2 is 'Признак 2';

comment on column trailer_models.trailer_name is 'Наименование';
comment on column trailer_models.cap is 'Вместимость';
comment on column trailer_models.carcap is 'Грузоподъемность';
comment on column trailer_models.p1 is 'Признак 1';
comment on column trailer_models.p2 is 'Признак 2';

comment on column atp.atp_name is 'Наименование автотранспортного предприятия';

comment on column ak.ak_name is 'Наименование автоколонны';

comment on column gps_units.call_sign is 'Позывной';

comment on column sensor.sens_name is 'Наименование датчика (температура, открытие дверей и тд)';

comment on column drivers.fname is 'Фамилия';
comment on column drivers.iname is 'Имя';
comment on column drivers.oname is 'Отчество';
comment on column drivers.address is 'Адрес';
comment on column drivers.phone is 'Телефон';
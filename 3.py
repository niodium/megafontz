# Склонение товара
num = int(input('Введите число:\n'))


def goods(num):
    word = ''
    anum = num % 100

    if num > 0:
        if anum in range(5, 21):
            word = 'товаров'
        else:
            if anum % 10 == 1:
                word = 'товар'
            elif anum % 10 in range(2, 5):
                word = 'товара'
            else:
                word = 'товаров'

    else:
        print('Число должно быть положительное')

    return str(num) + ' ' + word


if __name__ == '__main__':
    print(goods(num))

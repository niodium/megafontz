# Euler Problem 28
def diagsum(n): return (16 * pow(n, 3) + 26 * n) / 3 + 10 * pow(n, 2) + 1


if __name__ == '__main__':
    print('Сумма диагоналей матрицы 5х5:', diagsum(2))
    print('Сумма диагоналей матрицы 1001х1001:', diagsum(500))
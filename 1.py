# Ипотека
def overpayment():
    csum = 2000000
    i = 0.1 / 12
    t = 60
    return round(t * (csum * (i * pow(1 + i, t)) / (pow(1 + i, t) - 1)) - csum, 2)


if __name__ == '__main__':
    print(overpayment())

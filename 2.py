# Шифр Цезаря
# 2 алфавита en & ru
# кодировка utf-8
import string

mode = int(input('Для шифрования: 1, для дешифровки: 2\n'))
enLow = string.ascii_lowercase
enUp = string.ascii_uppercase
ruLow = 'абвгдеёжзийклмнопрстуфхцчшщъыьэюя'
ruUp = ruLow.upper()


def codec(data, step):
    res = ''

    for sign in data:
        if sign in enLow:
            res += enLow[(enLow.index(sign) + step) % len(enLow)]
        elif sign in enUp:
            res += enUp[(enUp.index(sign) + step) % len(enUp)]
        elif sign in ruLow:
            res += ruLow[(ruLow.index(sign) + step) % len(ruLow)]
        elif sign in ruUp:
            res += ruUp[(ruUp.index(sign) + step) % len(ruUp)]
        else:
            res += sign

    return res


def encode():
    step = 3

    try:
        with open('text.txt', 'r', encoding='utf-8') as f:
            data = f.read()

        res = codec(data, step)

        with open('text_encrypted.txt', 'wb') as ef:
            ef.write(res.encode('utf-8'))
            ef.close()
        print('Создан файл text_encrypted.txt')
    except OSError:
        print('Файл text.txt не существует')


def decode():
    step = -3

    try:
        with open('text_encrypted.txt', 'r', encoding='utf-8') as f:
            data = f.read()

        res = codec(data, step)

        with open('text_decrypted.txt', 'wb') as ef:
            ef.write(res.encode('utf-8'))
            ef.close()
        print('Создан файл text_decrypted.txt')
    except OSError:
        print('Файл text_encrypted.txt не существует')


if __name__ == '__main__':
    if mode == 1:
        encode()
    elif mode == 2:
        decode()
    else:
        print('Неверный выбор')